import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import  Axios from 'axios';
class App extends Component {
  onClickME=()=>{
    Axios.get('http://devopstest.aavgo.com/').then(res=>{
      alert(res.data)
    })
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Client Call</h1>
        </header>
        <p className="App-intro">
          <button onClick={this.onClickME}>Click Here</button>
        </p>
      </div>
    );
  }
}

export default App;
